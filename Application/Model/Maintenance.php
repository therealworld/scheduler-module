<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SchedulerModule\Application\Model;

use TheRealWorld\SchedulerModule\Application\Controller\Scheduler;

/**
 * Scheduler Maintenance class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\Maintenance
 */
class Maintenance extends Maintenance_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function execute()
    {
        // We overwrite the maintenance execute method without execute
        // the parent, because we create a task for updateing the
        // upcoming prices (primary goal of parent method)

        $oScheduler = oxNew(Scheduler::class);
        $mResult = $oScheduler->run(false);
        if (is_string($mResult) && !empty($mResult)) {
            echo $mResult;
        }
    }
}
