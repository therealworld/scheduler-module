<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SchedulerModule\Application\Model;

use Exception;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * SchedulerTasks class.
 */
class SchedulerTask extends BaseModel
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_blUseLazyLoading = true;

    protected bool $_blDisableShopCheck = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sClassName = '\TheRealWorld\SchedulerModule\Application\Model\SchedulerTask';

    /** Current task class name */
    protected ?string $_sTaskClassName = null;

    /** Current task title */
    protected ?string $_sTaskTitle = null;

    /** Current default task crontab */
    protected ?string $_sDefaultCrontab = null;

    /** Current task crontab */
    protected ?string $_sCrontab = null;

    /** Namespace Classname */
    protected ?string $_sNamespaceClassName = null;

    /** Current module path */
    protected ?string $_sModulePath = null;

    /** Current task is set active ? */
    protected ?bool $_bSetActive = null;

    /** Current task is set manually ? */
    protected ?bool $_bSetManually = null;

    /** The Task Object */
    protected ?object $_oTask = null;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('trwschedulertasks');
    }

    /** get is the task in progress */
    public function getProcessStatus(): bool
    {
        return $this->getTRWBoolData('oxinprogress') && $this->getId();
    }

    /** get the Name of the task */
    public function getTaskClassName(): string
    {
        if (is_null($this->_sTaskClassName)) {
            $this->_sTaskClassName = $this->getTRWStringData('oxtask');
        }

        return $this->_sTaskClassName;
    }

    /** get the Title of the task */
    public function getTaskTitle(): string
    {
        if (is_null($this->_sTaskTitle)) {
            $this->_sTaskTitle = Registry::getLang()->translateString(Str::getStr()->strtoupper(
                $this->getTaskClassName()
            ));
        }

        return $this->_sTaskTitle;
    }

    /** get the Crontab of the task */
    public function getCrontab(): string
    {
        if (is_null($this->_sCrontab) && !$this->_sCrontab = $this->getTRWStringData('oxcrontab')) {
            $this->_sCrontab = $this->getDefaultCrontab();
        }

        return $this->_sCrontab;
    }

    /** get the Path of the module (with task) */
    public function getDefaultCrontab(): string
    {
        if (is_null($this->_sDefaultCrontab)) {
            $this->_sDefaultCrontab = '';
            if (!is_null($this->_oTask)) {
                $this->_sDefaultCrontab = $this->_oTask->getDefaultCrontab();
            }
        }

        return $this->_sDefaultCrontab;
    }

    /** get the Path of the module (with task) */
    public function getModulePath(): string
    {
        if (is_null($this->_sModulePath)) {
            $this->_sModulePath = $this->getTRWStringData('oxmodulepath');
        }

        return $this->_sModulePath;
    }

    /** get the Namespace Class Name of the module */
    public function getNamespaceClassName(): string
    {
        if (is_null($this->_sNamespaceClassName)) {
            $this->_sNamespaceClassName = $this->getTRWStringData('oxnamespaceclassname');
        }

        return $this->_sNamespaceClassName;
    }

    /** is the Task set active? */
    public function isSetActive(): bool
    {
        if (is_null($this->_bSetActive)) {
            $this->_bSetActive = $this->getTRWBoolData('oxactive');
        }

        return $this->_bSetActive;
    }

    /** is the Task a manually Task? */
    public function isSetManually(): bool
    {
        if (is_null($this->_bSetManually)) {
            $this->_bSetManually = false;

            // either the task is set to manual,
            // or there is no default crontab defined
            if (
                (
                    !is_null($this->_oTask)
                    && !$this->getDefaultCrontab()
                )
                || $this->getTRWBoolData('oxmanually')
            ) {
                $this->_bSetManually = true;
            }
        }

        return $this->_bSetManually;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function load($sOxId = ''): bool
    {
        if ($bResult = parent::load($sOxId)) {
            $this->getTaskObjByNamespaceClassName(
                $this->getTRWStringData('oxnamespaceclassname')
            );
        }

        return $bResult;
    }

    /**
     * get the Obj of the task.
     *
     * @param string $sTaskClassName - Name of a Task
     * @param string $sModulePath    - Path of a Module (with Task)
     */
    public function getTaskObj(string $sTaskClassName = '', string $sModulePath = ''): ?object
    {
        if (is_null($this->_oTask)) {
            if (empty($sTaskClassName) && empty($sModulePath)) {
                $this->_oTask = $this->getTaskObjByNamespaceClassName();
            } else {
                $this->_sTaskClassName = $sTaskClassName;
                $this->_sModulePath = $sModulePath;

                $oConfig = Registry::getConfig();
                $sModuleBasePath = $oConfig->getConfigParam('sShopDir') . $oConfig->getModulesDir(false);
                $sFilePath =
                    $sModuleBasePath .
                    $this->_sModulePath .
                    '/SchedulerTasks/' .
                    $this->_sTaskClassName .
                    '.php';

                if (file_exists($sFilePath)) {
                    $aDeclaredClasses = get_declared_classes();

                    include_once $sFilePath;
                    $aDiff = array_diff(get_declared_classes(), $aDeclaredClasses);
                    $this->_sNamespaceClassName = array_pop($aDiff);
                    $this->_loadTaskObjByNamespaceClassName();
                }
            }
        }

        return $this->_oTask;
    }

    /**
     * get the Obj of the task.
     *
     * @param string $sNamespaceClassName - Name of a Namespace Class Task
     */
    public function getTaskObjByNamespaceClassName(string $sNamespaceClassName = ''): ?object
    {
        if (is_null($this->_oTask)) {
            $this->_sNamespaceClassName = ($sNamespaceClassName ?: $this->getNamespaceClassName());

            $this->_loadTaskObjByNamespaceClassName();
        }

        return $this->_oTask;
    }

    /** get the Current Step */
    public function getCurrentStep($bSub = false): int
    {
        $sStepCol = 'oxcurrent' . ($bSub ? 'sub' : '') . 'step';

        return $this->getTRWIntData($sStepCol);
    }

    /**
     * set the task in progress.
     *
     * @param bool $bIsRunManually  - is this a Process run manually?
     * @param int  $iNextStart      - is > 0 then set the timestamp as nextstart
     * @param int  $iCurrentStep    - the next current step
     * @param int  $iCurrentSubStep - the next current sub step
     *
     * @throws Exception
     */
    public function setProcessStatus(
        bool $bStatus = false,
        bool $bIsRunManually = false,
        int $iNextStart = 0,
        int $iCurrentStep = 0,
        int $iCurrentSubStep = 0
    ): bool {
        $result = false;
        if ($this->getId()) {
            $aParams = [
                'oxinprogress'     => ($bStatus ? 1 : 0),
                'oxisrunmanually'  => ($bIsRunManually ? 1 : 0),
                'oxcurrentstep'    => $iCurrentStep,
                'oxcurrentsubstep' => $iCurrentSubStep,
            ];
            if ($iNextStart) {
                $aParams['oxnextstart'] = date('Y-m-d H:i:s', $iNextStart);
            }
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'trwschedulertasks');
            $this->assign($aParams);
            $this->save();
            $result = true;
        }

        return $result;
    }

    /** get the actual Task.
     * @throws DatabaseConnectionException
     */
    public function loadActualTask(): ?SchedulerTask
    {
        $mResult = null;
        if (!$this->getId()) {
            $sOxId = $this->getNextPossibleCronjobOxId();

            if ($sOxId) {
                $this->load($sOxId);
                $mResult = $this;
            }
        }

        return $mResult;
    }

    /**
     * load a Task manually.
     *
     * @param string $sOxId of Task
     */
    public function loadTaskManually(string $sOxId = ''): ?SchedulerTask
    {
        $mResult = null;
        if ($this->getId()) {
            $mResult = $this;
        } elseif ($sOxId) {
            $this->load($sOxId);
            $mResult = $this;
        }

        return $mResult;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getSqlActiveSnippet($blForceCoreTable = null)
    {
        $sTable = $this->getViewName($blForceCoreTable);

        return " {$sTable}.oxactive = 1 ";
    }

    /**
     * calculate the next possible Cronjob, Result is OxId of Task.
     *
     * @param bool $bWithFutureTask - try also get a task in the future
     *
     * @throws DatabaseConnectionException
     */
    public function getNextPossibleCronjobOxId(bool $bWithFutureTask = false): string
    {
        $sResult = '';

        if (Registry::getConfig()->getConfigParam('bTRWSchedulerCronjobActive')) {
            $oDb = DatabaseProvider::getDb(true);

            $aSelect = [];

            // The possible in progress Task
            $aSelect[] = "select `oxid`
                from `trwschedulertasks`
                where `oxactive` = 1 and `oxinprogress` = 1
                 and `oxmanually` = 0 and `oxcrontab` != ''
                limit 1";

            // The oldest, urgent Task
            $aSelect[] = "select `oxid`
                from `trwschedulertasks`
                where `oxnextstart` < now() and `oxactive` = 1
                 and `oxinprogress` = 0 and `oxmanually` = 0 and `oxcrontab` != ''
                order by oxnextstart
                limit 1";

            // The next Task in the Future. This is only neccessary if you want to show the "next" possible Task
            if ($bWithFutureTask) {
                $aSelect[] = "select `oxid`
                    from `trwschedulertasks`
                    where `oxnextstart` > now() and `oxactive` = 1 
                    and `oxinprogress` = 0 and `oxmanually` = 0 and `oxcrontab` != ''
                    order by oxnextstart
                    limit 1";
            }

            $iSelectCount = 0;
            do {
                $sOxId = $oDb->getOne($aSelect[$iSelectCount]);
                $iSelectCount++;
            } while (!$sOxId && $iSelectCount !== count($aSelect));

            if ($sOxId) {
                $sResult = $sOxId;
            }
        }

        return $sResult;
    }

    /** load the Obj of the task */
    protected function _loadTaskObjByNamespaceClassName(): void
    {
        if ($this->_sNamespaceClassName) {
            try {
                $this->_oTask = oxNew($this->_sNamespaceClassName);
            } catch (Exception $e) {
                $this->delete();
            }
        }
    }
}
