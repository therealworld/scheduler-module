<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SchedulerModule\Application\Model;

use OxidEsales\Eshop\Core\Model\ListModel;

/**
 * SchedulerTasks list manager.
 */
class SchedulerTaskList extends ListModel
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sObjectsInListName = 'TheRealWorld\SchedulerModule\Application\Model\SchedulerTask';

    /** get a list of all Tasks from DB */
    public function getTaskList(): void
    {
        $oBaseObject = $this->getBaseObject();
        $sSelectFields = $oBaseObject->getSelectFields();
        $sViewName = $oBaseObject->getViewName();

        $sSelect = "select {$sSelectFields}
            from {$sViewName}
            order by oxtask";

        $this->selectString($sSelect);
    }
}
