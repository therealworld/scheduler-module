<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SchedulerModule\Application\Controller\Admin;

use Cron\CronExpression;
use Exception;
use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\SchedulerModule\Application\Controller\Scheduler;
use TheRealWorld\SchedulerModule\Application\Model\SchedulerTask;
use TheRealWorld\SchedulerModule\Application\Model\SchedulerTaskList;
use TheRealWorld\ToolsModule\Core\ToolsDBLog;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;

/**
 * provides a Scheduler-Configuration
 * Admin Menu: Service -> Scheduler.
 */
class SchedulerController extends AdminDetailsController
{
    /** Next Cronjob SchedulerTask Obj */
    protected ?object $_oNextCronjobSchedulerTask = null;

    /** running SchedulerTask Obj */
    protected ?object $_oRunningSchedulerTask = null;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'scheduler.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        $oScheduler = oxNew(Scheduler::class);

        // running & progress task
        $bSchedulerIsRunning = $oScheduler->getSchedulerIsRunning();
        $this->_aViewData['bSchedulerIsRunning'] = $bSchedulerIsRunning;

        // task list
        if (!$bSchedulerIsRunning) {
            $oSchedulerTaskList = oxNew(SchedulerTaskList::class);
            $oSchedulerTaskList->getTaskList();
            $this->_aViewData['oSchedulerTaskList'] = $oSchedulerTaskList;
        }

        $this->_aViewData['trwschedulerurl'] = $this->getViewConfig()->getSelfLink() . 'cl=SchedulerController';

        // show logs
        if (ToolsLog::isLog()) {
            ToolsLog::setLogEntry(
                Registry::getLang()->translateString('TRWSCHEDULER_OUTPUT_ENDED'),
                __CLASS__ . ' - ' . __FUNCTION__
            );
            $this->_aViewData['sLog'] = ToolsLog::getLogAsText();
            ToolsLog::resetLogEntry();
            $this->_aViewData['iLogPage'] = ToolsDBLog::getLogPage();
        }

        return $this->_sThisTemplate;
    }

    /** Template Getter. Show Next Cronjob.
     * @throws DatabaseConnectionException
     */
    public function getNextCronjobSchedulerTaskObj(): ?object
    {
        $oScheduler = oxNew(Scheduler::class);

        if (is_null($this->_oNextCronjobSchedulerTask)) {
            $oConfig = Registry::getConfig();

            if (
                !$oScheduler->getSchedulerIsRunning()
                && $oConfig->getConfigParam('bTRWSchedulerCronjobActive')
            ) {
                $oSchedulerTask = oxNew(SchedulerTask::class);
                if ($sOxId = $oSchedulerTask->getNextPossibleCronjobOxId(true)) {
                    $oSchedulerTask->load($sOxId);
                    $this->_oNextCronjobSchedulerTask = $oSchedulerTask;
                }
            }
        }

        return $this->_oNextCronjobSchedulerTask;
    }

    /** Template Getter. Show actual running Cronjob */
    public function getRunningTaskObj(): ?object
    {
        if (
            is_null($this->_oRunningSchedulerTask)
            && $sOxId = ToolsDB::getAnyId('trwschedulertasks', ['oxinprogress' => 1])
        ) {
            $oSchedulerTask = oxNew(SchedulerTask::class);
            $oSchedulerTask->load($sOxId);
            $this->_oRunningSchedulerTask = $oSchedulerTask;
        }

        return $this->_oRunningSchedulerTask;
    }

    /** FNC run Task Reset */
    public function runTaskReset(): void
    {
        $oScheduler = oxNew(Scheduler::class);
        $oScheduler->setSchedulerIsRunning(false);

        try {
            $oScheduler->setTaskReset(Registry::getRequest()->getRequestParameter('oxid'));
        } catch (Exception $e) {
        }
    }

    /** FNC run Task Manually */
    public function runTaskManually(): void
    {
        $oScheduler = oxNew(Scheduler::class);
        $oScheduler->runTaskManually(Registry::getRequest()->getRequestParameter('oxid'));
    }

    /** FNC upload file */
    public function uploadFile(): void
    {
        $oSchedulerTask = oxNew(SchedulerTask::class);
        $oSchedulerTask->loadTaskManually(Registry::getRequest()->getRequestParameter('oxid'));
        $oTaskObj = $oSchedulerTask->getTaskObj();

        ToolsConfig::uploadFile('manuallyUploadFile', $oTaskObj->getPathForManualFiles());
    }

    /**
     * FNC Set Options.
     *
     * @throws Exception
     */
    public function runSetOptions(): void
    {
        $oLang = Registry::getLang();
        $oRequest = Registry::getRequest();
        $aOptions = $oRequest->getRequestParameter('editval');
        $sOxId = $oRequest->getRequestParameter('oxid');

        $oSchedulerTask = oxNew(SchedulerTask::class);
        if ($oSchedulerTask->load($sOxId)) {
            $iNextStart = 0;
            $sNextStart = '';
            if ($sCrontab = $aOptions['oxcrontab']) {
                // validity check of crontab
                try {
                    $cron = new CronExpression($sCrontab);
                    $iNextStart = $cron->getNextRunDate()->getTimestamp();
                } catch (Exception $e) {
                    $sCrontab = $oSchedulerTask->getCrontab();
                    ToolsLog::setLogEntry(
                        $oLang->translateString('TRWSCHEDULER_CRONTAB_NOT_VALID'),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                }
                $sNextStart = date('Y-m-d H:i:s', $iNextStart);
            }

            $aParams = [
                'oxactive'    => $aOptions['oxactive'] ?? 0,
                'oxmanually'  => $aOptions['oxmanually'] ?? 0,
                'oxnextstart' => $sNextStart,
                'oxcrontab'   => $sCrontab,
            ];
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'trwschedulertasks');
            $oSchedulerTask->assign($aParams);
            $oSchedulerTask->save();
            ToolsLog::setLogEntry(
                $oLang->translateString('TRWSCHEDULER_SET_OPTION'),
                __CLASS__ . ' - ' . __FUNCTION__
            );
        }
    }

    /**
     * FNC run Show DB Log.
     *
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function runShowLog(): void
    {
        ToolsLog::setLogEntry(
            Registry::getLang()->translateString('TRWSCHEDULER_LOG_SHOW') . '...',
            __CLASS__ . ' - ' . __FUNCTION__
        );
        $iPage = Registry::getRequest()->getRequestParameter('page');
        $iPage = max((int) $iPage, 0);
        ToolsDBLog::loadLogFromDb($iPage);
    }

    /** FNC run Clean DB Log */
    public function runCleanLog(): void
    {
        ToolsDBLog::cleanLogFromDb();
        ToolsLog::setLogEntry(
            Registry::getLang()->translateString('TRWSCHEDULER_LOG_CLEAN') . '...',
            __CLASS__ . ' - ' . __FUNCTION__
        );
    }

    /** FNC run search new tasks */
    public function runSearchNewTasks(): void
    {
        ToolsLog::setLogEntry(
            Registry::getLang()->translateString('TRWSCHEDULER_SEARCH_TASKS') . '...',
            __CLASS__ . ' - ' . __FUNCTION__
        );
        $oScheduler = oxNew(Scheduler::class);
        $oScheduler->getNewTasks();
    }

    /** FNC run Delete not active tasks */
    public function runDeleteNotActiveTasks(): void
    {
        ToolsLog::setLogEntry(
            Registry::getLang()->translateString('TRWSCHEDULER_DELETE_TASKS') . '...',
            __CLASS__ . ' - ' . __FUNCTION__
        );
        $sDelete = 'delete from `trwschedulertasks`
                where `oxactive` = 0';
        ToolsDB::execute($sDelete);
    }
}
