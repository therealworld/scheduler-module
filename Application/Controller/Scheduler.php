<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SchedulerModule\Application\Controller;

use Cron\CronExpression;
use Exception;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Module\ModuleList;
use OxidEsales\Eshop\Core\Registry;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\SchedulerModule\Application\Model\SchedulerTask;
use TheRealWorld\ToolsModule\Core\ToolsDBLog;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;

/**
 * Scheduler-Class to manage the tasks.
 */
class Scheduler
{
    /** Object of the schedule Task */
    protected ?object $_oSchedulerTask = null;

    /** The possible runtime of the script */
    protected ?int $_iScriptRunTime = null;

    /** The default runtime of the script */
    protected int $_iScriptRunTimeDefault = 30;

    /** Name of the Oxid-Config should i check if Scheduler is running */
    private string $_sConfigCheckStillRunning = 'bTRWSchedulerCheckStillRunning';

    /** Name of the Oxid-Config is Scheduler is running */
    private string $_sConfigStillRunning = 'bTRWSchedulerStillRunning';

    /**
     * Run the Scheduler.
     *
     * @param $bAnswer - should the scheduler say something
     *
     * @return string - done, busy, nothing to do
     *
     * @throws DatabaseConnectionException
     */
    public function run(bool $bAnswer = true): string
    {
        if (
            !$this->getSchedulerIsRunning()
            && Registry::getConfig()->getConfigParam('bTRWSchedulerCronjobActive')
        ) {
            $this->_oSchedulerTask = oxNew(SchedulerTask::class);

            if ($this->_oSchedulerTask->loadActualTask()) {
                $this->runActualTask();
                $sResult = 'done';
            } else {
                $sResult = 'nothing to do';
            }
        } else {
            $sResult = 'busy';
        }

        // write the log
        ToolsDBLog::writeLogToDb();

        if (!$bAnswer) {
            $sResult = '';
        }

        return $sResult;
    }

    /** look for new Tasks and install it */
    public function getNewTasks(): void
    {
        $oConfig = Registry::getConfig();
        $oLang = Registry::getLang();
        $oModuleList = oxNew(ModuleList::class);
        $aModules = $oModuleList->getActiveModuleInfo();
        $sModuleBasePath = $oConfig->getConfigParam('sShopDir') . $oConfig->getModulesDir(false);

        foreach ($aModules as $sModulePath) {
            $sTaskPath = $sModuleBasePath . $sModulePath . '/SchedulerTasks/';
            if (is_dir($sTaskPath) && ($aTaskFiles = ToolsFile::getFileNamesFromPath($sTaskPath, '/^task.*\.php$/i'))) {
                foreach ($aTaskFiles as $sTaskFile) {
                    $sTaskClassName = str_replace('.php', '', $sTaskFile);

                    try {
                        $oSchedulerTask = oxNew(SchedulerTask::class);

                        $oTask = $oSchedulerTask->getTaskObj($sTaskClassName, $sModulePath);

                        $sTaskTitle = $oSchedulerTask->getTaskTitle();
                        $sNamespaceClassName = $oSchedulerTask->getNamespaceClassName();

                        $aParams = [
                            'oxtask'               => $sTaskClassName,
                            'oxmodulepath'         => $sModulePath,
                            'oxnamespaceclassname' => $sNamespaceClassName,
                        ];

                        if (!($sOxId = ToolsDB::getAnyId('trwschedulertasks', ['oxtask' => $sTaskClassName]))) {
                            $sCrontab = $oSchedulerTask->getCrontab();

                            $sNextStart = '';
                            if ($sCrontab) {
                                try {
                                    $cron = new CronExpression($sCrontab);
                                    $sNextStart = $cron->getNextRunDate()->format('Y-m-d H:i:s');
                                } catch (Exception $e) {
                                    $sCrontab = '';
                                    ToolsLog::setLogEntry(
                                        $oLang->translateString('TRWSCHEDULER_CRONTAB_NOT_VALID'),
                                        __CLASS__ . ' - ' . __FUNCTION__,
                                        'error'
                                    );
                                }
                            }

                            $aParams += [
                                'oxactive'    => 0,
                                'oxmanually'  => ($sNextStart ? 0 : 1),
                                'oxnextstart' => $sNextStart,
                                'oxcrontab'   => $sCrontab,
                            ];

                            $oTask->install();

                            ToolsLog::setLogEntry(
                                sprintf(
                                    $oLang->translateString('TRWSCHEDULER_NEWTASK_INSTALL'),
                                    $sTaskTitle
                                ),
                                $sTaskClassName
                            );
                        } else {
                            $oSchedulerTask->load($sOxId);
                        }

                        $aParams = ToolsDB::convertDB2OxParams($aParams, 'trwschedulertasks');

                        $oSchedulerTask->assign($aParams);
                        $oSchedulerTask->save();
                    } catch (Exception $e) {
                        $sMessage = $e->getMessage();
                        ToolsLog::setLogEntry($sMessage, $sTaskClassName, 'error');
                    }
                }
            }
        }
    }

    /**
     * run a Task manually.
     *
     * @param string $sOxId of Task
     */
    public function runTaskManually(string $sOxId = ''): bool
    {
        $bResult = false;

        $this->_oSchedulerTask = oxNew(SchedulerTask::class);
        if ($this->_oSchedulerTask->loadTaskManually($sOxId)) {
            $bResult = $this->runActualTask(true);
        }

        // write the log
        ToolsDBLog::writeLogToDb();

        return $bResult;
    }

    /** get the scheduler is running */
    public function getSchedulerIsRunning(): bool
    {
        $oConfig = Registry::getConfig();

        return $oConfig->getConfigParam($this->_sConfigCheckStillRunning)
            && $oConfig->getConfigParam($this->_sConfigStillRunning);
    }

    /**
     * set the scheduler is running.
     *
     * @param string $sOxId of Task
     *
     * @throws Exception
     */
    public function setTaskReset(string $sOxId = ''): void
    {
        $this->_oSchedulerTask = oxNew(SchedulerTask::class);
        if ($this->_oSchedulerTask->loadTaskManually($sOxId)) {
            $this->_oSchedulerTask->setProcessStatus(false, true);
        }
    }

    /**
     * set the scheduler is running.
     */
    public function setSchedulerIsRunning(bool $bStatus = true): void
    {
        $oSession = Registry::getSession();

        try {
            ToolsConfig::saveConfigParam($this->_sConfigStillRunning, $bStatus, 'bool', 'trwscheduler');
        } catch (ContainerExceptionInterface | NotFoundExceptionInterface $e) {
        }
        if ($bStatus) {
            $iStopTimeStamp = $this->getScriptRunTime() + time();
            $oSession->setVariable('iTRWSchedulerStopTimeStamp', $iStopTimeStamp);
        } else {
            $oSession->deleteVariable('iTRWSchedulerStopTimeStamp');
        }
    }

    /**
     * run the actual Task.
     *
     * @param bool $bRunManually - run the task manually?
     */
    protected function runActualTask(bool $bRunManually = false): bool
    {
        $bResult = false;
        $oLang = Registry::getLang();

        // First we set the Scheduler as "running"
        $this->setSchedulerIsRunning();

        if ($this->_oSchedulerTask) {
            $sTaskTitle = $this->_oSchedulerTask->getTaskTitle();
            $sTaskClassName = $this->_oSchedulerTask->getTaskClassName();

            // We check the process-Status of the Task and set it maybe "in process" and log a "run"
            $bProcessStatus = $this->_oSchedulerTask->getProcessStatus();
            if ($bProcessStatus === false) {
                $this->_oSchedulerTask->setProcessStatus(true, $bRunManually);
                ToolsLog::setLogEntry(
                    sprintf(
                        $oLang->translateString('TRWSCHEDULER_TASK_RUN'),
                        $sTaskTitle
                    ),
                    $sTaskClassName
                );
            }

            if ($oTask = $this->_oSchedulerTask->getTaskObj()) {
                $iNextStep = $this->_oSchedulerTask->getCurrentStep();
                $iNextSubStep = $this->_oSchedulerTask->getCurrentStep(true);
                if ($iNextStep > 0 || $iNextSubStep > 0) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            $oLang->translateString('TRWSCHEDULER_TASK_RUN_AGAIN'),
                            $iNextStep,
                            $iNextSubStep
                        ),
                        $sTaskClassName
                    );
                }

                // Run the Task
                $oTask->run(
                    $iNextStep,
                    $iNextSubStep,
                    $bRunManually
                );

                // As result of the run, set the next start
                $iNextStep = $oTask->getNextStep();
                $iNextSubStep = $oTask->getNextStep(true);
                if ($iNextStep > 0 || $iNextSubStep > 0) {
                    $this->_oSchedulerTask->setProcessStatus(true, $bRunManually, 0, $iNextStep, $iNextSubStep);
                } else {
                    $iNextStart = 0;
                    if ($this->_oSchedulerTask->getCrontab()) {
                        try {
                            $cron = new CronExpression($this->_oSchedulerTask->getCrontab());
                            $iNextStart = $cron->getNextRunDate()->getTimestamp();
                        } catch (Exception $e) {
                            $iNextStart = false;
                            ToolsLog::setLogEntry(
                                $oLang->translateString('TRWSCHEDULER_CRONTAB_NOT_VALID'),
                                __CLASS__ . ' - ' . __FUNCTION__,
                                'error'
                            );
                        }
                    }
                    $this->_oSchedulerTask->setProcessStatus(false, $bRunManually, $iNextStart);
                }

                // We check the process-Status of the Task. Is everything ok, we log a "stop"
                if ($this->_oSchedulerTask->getProcessStatus() === false) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            $oLang->translateString('TRWSCHEDULER_TASK_STOP'),
                            $sTaskTitle
                        ),
                        $sTaskClassName
                    );
                } elseif ($iNextStep > 0 || $iNextSubStep > 0) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            $oLang->translateString('TRWSCHEDULER_TASK_TO_BE_CONTINUED'),
                            $iNextStep,
                            $iNextSubStep
                        ),
                        $sTaskClassName
                    );
                }
                $bResult = true;
            } else {
                $this->deleteTask();
            }
        }

        // Last we set the Scheduler as "not running"
        $this->setSchedulerIsRunning(false);

        return $bResult;
    }

    /** get the path of the tasks */
    protected function deleteTask(): void
    {
        if ($this->_oSchedulerTask) {
            $this->_oSchedulerTask->delete();
            $this->_oSchedulerTask = null;
        }
    }

    /** Get script run time, depending on how the script was called */
    private function getScriptRunTime(): int
    {
        if (is_null($this->_iScriptRunTime)) {
            $sType = (PHP_SAPI === 'cli' ? 'Console' : 'Browser');
            $iScriptRunTime = (float) Registry::getConfig()->getConfigParam('iTRWSchedulerRunTime' . $sType);
            $iScriptRunTime = $iScriptRunTime > 0 ? $iScriptRunTime : $this->_iScriptRunTimeDefault;
            $this->_iScriptRunTime = (int) round($iScriptRunTime * 0.9);
        }

        return $this->_iScriptRunTime;
    }
}
