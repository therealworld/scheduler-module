<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TASKDUMMY'                => 'Hallo Welt!',
    'TASKUPDATEUPCOMINGPRICES' => 'Aktualisierung vorbereitete Preise',

    'TRWSCHEDULER_NEWTASK_INSTALL'      => 'NEUE AUFGABE INSTALLIERT: %s',
    'TRWSCHEDULER_TASK_RUN'             => 'AUFGABE GESTARTET: %s',
    'TRWSCHEDULER_TASK_RUN_AGAIN'       => 'AUFGABE WIRD FORTGESETZT BEI SCHRITT %s / %s',
    'TRWSCHEDULER_TASK_TO_BE_CONTINUED' => 'AUFGABE PAUSIERT BEI SCHRITT %s / %s',
    'TRWSCHEDULER_TASK_STOP'            => 'AUFGABE GESTOPPT: %s',
];
