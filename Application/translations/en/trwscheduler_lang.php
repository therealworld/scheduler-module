<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TASKDUMMY'                => 'Hello World!',
    'TASKUPDATEUPCOMINGPRICES' => 'Update Upcoming Prices',

    'TRWSCHEDULER_NEWTASK_INSTALL'      => 'NEW TASK INSTALLED: %s',
    'TRWSCHEDULER_TASK_RUN'             => 'TASK STARTED: %s',
    'TRWSCHEDULER_TASK_RUN_AGAIN'       => 'TASK RUN AGAIN ON STEP %s / %s',
    'TRWSCHEDULER_TASK_TO_BE_CONTINUED' => 'TASK PAUSES AT STEP %s / %s',
    'TRWSCHEDULER_TASK_STOP'            => 'TASK STOPED: %s',
];
