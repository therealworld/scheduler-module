[{include file="headitem.tpl" title="TRWSCHEDULER"|oxmultilangassign}]
[{assign var="oNextCronjobSchedulerTaskObj" value=$oView->getNextCronjobSchedulerTaskObj()}]
[{assign var="oRunningTaskObj" value=$oView->getRunningTaskObj()}]
[{assign var="sRunningTaskTitle" value=""}]
[{if $oRunningTaskObj}]
    [{assign var="sRunningTaskTitle" value=$oRunningTaskObj->getTaskTitle()}]
    [{assign var="iCurrentStep" value=$oRunningTaskObj->getCurrentStep()}]
    [{assign var="iCurrentSubStep" value=$oRunningTaskObj->getCurrentStep(true)}]
    [{if $iCurrentStep || $iCurrentSubStep}]
        [{assign var="sRunningTaskTitle" value=$sRunningTaskTitle|cat:" ("|cat:$iCurrentStep|cat:" / "|cat:$iCurrentSubStep|cat:")"}]
    [{/if}]
    [{capture assign="taskResetSnippet"}]
        <form name="editreset" action="[{$oViewConf->getSelfLink()}]" method="post">
            [{$oViewConf->getHiddenSid()}]
            <input type="hidden" name="cl" value="schedulercontroller" />
            <input type="hidden" name="fnc" value="runTaskReset" />
            <input type="hidden" name="oxid" value="[{$oRunningTaskObj->getId()}]" />
            <input type="submit" name="submitbuttonreset" class="small" value="[{oxmultilang ident="TRWSCHEDULER_RESET"}]" title="[{oxmultilang ident="HELP_TRWSCHEDULER_RESET"}]" />
            [{oxinputhelp ident="HELP_TRWSCHEDULER_RESET"}]
        </form>
    [{/capture}]
[{/if}]

<h1>[{oxmultilang ident="TRWSCHEDULER"}]</h1>

<div id="trwscheduler_wrap">
    <h3>[{oxmultilang ident="TRWSCHEDULER"}]</h3>
    [{if !$bSchedulerIsRunning}]
        <ul>
            <li>
                [{oxmultilang ident="TRWSCHEDULER_LOG" suffix="COLON"}]
                <a href="[{$oViewConf->getSelfLink()}]cl=schedulercontroller&fnc=runShowLog">[{oxmultilang ident="TRWSCHEDULER_LOG_SHOW"}]</a>
            </li>
            <li>
                [{oxmultilang ident="TRWSCHEDULER_LOG" suffix="COLON"}]
                <a href="[{$oViewConf->getSelfLink()}]cl=schedulercontroller&fnc=runCleanLog">[{oxmultilang ident="TRWSCHEDULER_LOG_CLEAN"}]</a>
            </li>
            <li>[{oxmultilang ident="TRWSCHEDULER" suffix="COLON"}]
                <a href="[{$oViewConf->getSelfLink()}]cl=schedulercontroller&fnc=runSearchNewTasks">[{oxmultilang ident="TRWSCHEDULER_SEARCH_TASKS"}]</a>
            </li>
            <li>[{oxmultilang ident="TRWSCHEDULER" suffix="COLON"}]
                <a href="[{$oViewConf->getSelfLink()}]cl=schedulercontroller&fnc=runDeleteNotActiveTasks">[{oxmultilang ident="TRWSCHEDULER_DELETE_TASKS"}]</a>
            </li>

            [{if $oNextCronjobSchedulerTaskObj != false}]
                <li>[{oxmultilang ident="TRWSCHEDULER_NEXTTASK" suffix="COLON"}]
                    <b>
                        [{$oNextCronjobSchedulerTaskObj->getTaskTitle()}] (
                        [{assign var="iNextCurrentStep" value=$oNextCronjobSchedulerTaskObj->getCurrentStep()}]
                        [{assign var="iNextCurrentSubStep" value=$oNextCronjobSchedulerTaskObj->getCurrentStep(true)}]
                        [{if $iNextCurrentStep || $iNextCurrentSubStep}]
                            [{$iCurrentStep}] / [{$iCurrentSubStep}] /
                        [{/if}]
                        [{$oNextCronjobSchedulerTaskObj->trwschedulertasks__oxnextstart->value}] )
                    </b>
                </li>
            [{/if}]
        </ul>
    [{/if}]

    <div id="runmanuallynote" class="errorbox none">
        [{oxmultilang ident="TRWSCHEDULER_INPROGRESS_MANUAL"}]
        <span class="loader"></span>
    </div>
    [{if $bSchedulerIsRunning}]
        <div class="errorbox">
            [{oxmultilang ident="TRWSCHEDULER_BUSY" args=$sRunningTaskTitle}]
            [{if $oRunningTaskObj}]
                <br />[{$taskResetSnippet}]
            [{/if}]
            <br /><a href="[{$oViewConf->getSelfLink()}]cl=schedulercontroller&fnc=runShowLog">[{oxmultilang ident="TRWSCHEDULER_LOG_SHOW"}]</a>
        </div>
    [{/if}]
    [{if $sLog}]
        <h3>[{oxmultilang ident="TRWSCHEDULER_OUTPUT"}]</h3>
        <pre>[{$sLog}]</pre>
        [{if $iLogPage}]
            <p>[{oxmultilang ident="TRWSCHEDULER_LOG"}]: <a href="[{$oViewConf->getSelfLink()}]cl=schedulercontroller&fnc=runShowLog&page=[{$iLogPage}]">[{oxmultilang ident="TRWSCHEDULER_LOG_SHOW_PAGE"}] [{$iLogPage}]</a></p>
        [{/if}]

        [{if $oRunningTaskObj && $oRunningTaskObj->isSetManually() && $oRunningTaskObj->isSetActive()}]
            <div>
                <form name="editrunmanually1" action="[{$oViewConf->getSelfLink()}]" method="post">
                    [{$oViewConf->getHiddenSid()}]
                    <input type="hidden" name="cl" value="schedulercontroller" />
                    <input type="hidden" name="fnc" value="runTaskManually" />
                    <input type="hidden" name="oxid" value="[{$oRunningTaskObj->getId()}]" />
                    <input id="buttonrunmanually" type="submit" name="submitbuttonrunmanually1" value="[{oxmultilang ident="TRWSCHEDULER_START_TASK_MANUALLY"}]" />
                </form>
                [{if $oViewConf->getConfigParam('bTRWSchedulerManualAutoReload')}]
                    <script language="javascript" type="text/javascript">
                        window.onload = function()
                        {
                            clickOnElement('buttonrunmanually');
                        };
                    </script>
                [{/if}]
            </div>
        [{/if}]
        <p>
            <a href="[{$trwschedulerurl}]">[{oxmultilang ident="TRWSCHEDULER_LIST"}]</a>
        </p>
    [{/if}]
    [{if !$bSchedulerIsRunning && !$sLog && $oSchedulerTaskList|@count}]
        <h3>[{oxmultilang ident="TRWSCHEDULER_TASKS"}]</h3>
        <table>
            <tr>
                <th scope="col" valign="top">[{oxmultilang ident="TRWSCHEDULER_TASKS"}]</th>
                <th scope="col" valign="top" class="text-left">[{oxmultilang ident="TRWSCHEDULER_CORESETTINGS"}]</th>
                <th scope="col" valign="top">[{oxmultilang ident="TRWSCHEDULER_FILEUPLOAD"}]</th>
                <th scope="col" valign="top" class="text-left">[{oxmultilang ident="TRWSCHEDULER_START_TASK_MANUALLY"}]</th>
            </tr>
            [{foreach from=$oSchedulerTaskList item="oTask"}]
                [{assign var="oTaskObj" value=$oTask->getTaskObj()}]
                [{if $oTaskObj}]
                    <tr>
                        <th scope="row">[{$oTask->getTaskTitle()}]</th>
                        <td>
                            [{* show cronjob options only, if a default crontab is defined *}]
                            <form class="taskoptions" name="editoptions" action="[{$oViewConf->getSelfLink()}]" method="post">
                                [{$oViewConf->getHiddenSid()}]
                                <input type="hidden" name="cl" value="schedulercontroller" />
                                <input type="hidden" name="fnc" value="runSetOptions" />
                                <input type="hidden" name="oxid" value="[{$oTask->getId()}]" />
                                <select name="editval[oxactive]">
                                    <option value="0"[{if !$oTask->isSetActive()}] selected[{/if}]>[{oxmultilang ident="TRWSCHEDULER_ACTIVE"}] [{oxmultilang ident="GENERAL_NO"}]</option>
                                    <option value="1"[{if $oTask->isSetActive()}] selected[{/if}]>[{oxmultilang ident="TRWSCHEDULER_ACTIVE"}] [{oxmultilang ident="GENERAL_YES"}]</option>
                                </select>
                                [{if $oTask->getDefaultCrontab()}]
                                    <select name="editval[oxmanually]">
                                        <option value="0"[{if !$oTask->isSetManually()}] selected[{/if}]>[{oxmultilang ident="TRWSCHEDULER_CRONJOB"}] [{oxmultilang ident="GENERAL_YES"}]</option>
                                        <option value="1"[{if $oTask->isSetManually()}] selected[{/if}]>[{oxmultilang ident="TRWSCHEDULER_CRONJOB"}] [{oxmultilang ident="GENERAL_NO"}]</option>
                                    </select>
                                    <input type="text" name="editval[oxcrontab]" value="[{$oTask->getCrontab()}]" size="10" maxlength="255" /> [{oxinputhelp ident="TRWSCHEDULER_CRONTAB_HELP"}]
                                [{else}]
                                    <input type="hidden" name="editval[oxcrontab]" value="" />
                                [{/if}]
                                <input type="submit" name="submitbuttonoptions" value="[{oxmultilang ident="GENERAL_SAVE"}]" />
                            </form>
                        </td>
                        [{assign var="bManualRunable" value=false}]
                        [{assign var="bManualReload" value=false}]
                        [{if $oTask->isSetManually() && $oTask->isSetActive()}]
                            [{assign var="bManualRunable" value=true}]
                            [{if $oRunningTaskObj && $oTask->getId() == $oRunningTaskObj->getId()}]
                                [{assign var="bManualReload" value=true}]
                            [{/if}]
                        [{/if}]
                        <td class="text-center">
                            [{if $bManualRunable && !$oTask->getDefaultCrontab() && $oTaskObj->getPathForManualFiles()}]
                                [{assign var="aManualFileList" value=$oTaskObj->getManualFileList()}]
                                [{if $aManualFileList|@count}]
                                    [{oxmultilang ident="TRWSCHEDULER_FILEUPLOADED" suffix="COLON"}]
                                    <b>
                                        [{foreach from=$aManualFileList item=sManualFile name=manualFileLoop}]
                                            [{if !$smarty.foreach.manualFileLoop.first}], [{/if}][{$sManualFile}]
                                        [{/foreach}]
                                    </b>
                                    [{else}]
                                    <form name="editupload" action="[{$oViewConf->getSelfLink()}]" method="post" enctype="multipart/form-data">
                                        [{$oViewConf->getHiddenSid()}]
                                        <input type="hidden" name="cl" value="SchedulerController" />
                                        <input type="hidden" name="fnc" value="uploadFile" />
                                        <input type="hidden" name="oxid" value="[{$oTask->getId()}]" />
                                        <input type="file" name="manuallyUploadFile" />
                                        <input type="submit" name="submitbuttonupload" value="[{oxmultilang ident="TRWSCHEDULER_UPLOAD"}]" />
                                        <input type="hidden" name="MAX_FILE_SIZE" value="[{$iMaxUploadFileSize}]" />
                                    </form>
                                [{/if}]
                            [{else}]-
                            [{/if}]
                        </td>
                        <td [{if $bManualReload}]class="manualrun"[{/if}]>
                            [{if $bManualRunable}]
                                <form name="editrunmanually2" action="[{$oViewConf->getSelfLink()}]" method="post">
                                    [{$oViewConf->getHiddenSid()}]
                                    <input type="hidden" name="cl" value="SchedulerController" />
                                    <input type="hidden" name="fnc" value="runTaskManually" />
                                    <input type="hidden" name="oxid" value="[{$oTask->getId()}]" />
                                    <input type="submit" name="submitbuttonrunmanually2" value="[{if $bManualReload}][{oxmultilang ident="TRWSCHEDULER_RELOAD_TASK_MANUALLY"}][{else}][{oxmultilang ident="TRWSCHEDULER_START_TASK_MANUALLY"}][{/if}]" onclick="showManualNote();" title="[{oxmultilang ident="HELP_TRWSCHEDULER_INPROGRESS"}]" />
                                </form>
                                [{if $bManualReload}]
                                    [{oxinputhelp ident="HELP_TRWSCHEDULER_INPROGRESS"}]
                                    [{$taskResetSnippet}]
                                [{/if}]
                            [{/if}]
                        </td>
                    </tr>
                [{/if}]
            [{/foreach}]
        </table>
    [{/if}]
</div>
[{include file="bottomitem.tpl"}]