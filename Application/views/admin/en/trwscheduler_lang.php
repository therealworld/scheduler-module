<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

include_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR .
    'en' .
    DIRECTORY_SEPARATOR . basename(__FILE__);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array_merge(
    $aLang,
    [
        'mxtrwscheduler' => 'Task-Scheduler',

        'TRWSCHEDULER'                      => 'Task-Scheduler',
        'TRWSCHEDULER_LOG'                  => 'Task-Scheduler-Log',
        'TRWSCHEDULER_LOG_SHOW'             => 'show Database-Log',
        'TRWSCHEDULER_LOG_SHOW_PAGE'        => 'show Database-Log Page',
        'TRWSCHEDULER_LOG_CLEAN'            => 'clean Database-Log',
        'TRWSCHEDULER_LOG_MAIL_SUBJECT'     => 'Task-Scheduler: important note',
        'TRWSCHEDULER_SEARCH_TASKS'         => 'look for new tasks',
        'TRWSCHEDULER_DELETE_TASKS'         => 'remove all inactive tasks',
        'TRWSCHEDULER_OPTIONS'              => 'Task-Options',
        'TRWSCHEDULER_TASKS'                => 'Tasks',
        'TRWSCHEDULER_BUSY'                 => 'The task scheduler is busy with the task "%s". Therefore it is not possible to start a task manually.',
        'TRWSCHEDULER_INPROGRESS_MANUAL'    => 'The task scheduler is busy with a manually started task and will restart itself if necessary.<br />Please have a moment of patience and read the log output.',
        'TRWSCHEDULER_OUTPUT'               => 'Output:',
        'TRWSCHEDULER_OUTPUT_ENDED'         => 'Output ended.',
        'TRWSCHEDULER_CORESETTINGS'         => 'Core Settings',
        'TRWSCHEDULER_START_TASK_MANUALLY'  => 'start manually',
        'TRWSCHEDULER_RELOAD_TASK_MANUALLY' => 'manuall reload',
        'TRWSCHEDULER_CRONJOB'              => 'Cronjob',
        'TRWSCHEDULER_CRONTAB'              => 'Crontab',
        'TRWSCHEDULER_CRONTAB_HELP'         => 'Format * * * * *, corresponds to the classic crontab format. The actual execution time may vary and depends on the server cronjob, which regularly triggers the task scheduler.',
        'TRWSCHEDULER_CRONTAB_NOT_VALID'    => 'Crontab not valid, set to default',
        'TRWSCHEDULER_ACTIVE'               => 'active',
        'TRWSCHEDULER_SET_OPTION'           => 'set options',
        'TRWSCHEDULER_LIST'                 => 'go to schedulerlist',
        'TRWSCHEDULER_RESET'                => 'Reset',
        'TRWSCHEDULER_NEXTTASK'             => 'Next potential Cronjob-Task',
        'TRWSCHEDULER_UPLOAD'               => 'upload',
        'TRWSCHEDULER_FILEUPLOAD'           => 'Fileupload',
        'TRWSCHEDULER_FILEUPLOADED'         => 'uploaded File(s)',

        'TRWSCHEDULER_TASK_UPDATEPRICES'  => 'update upcoming prices ended',
        'TRWSCHEDULER_TASK_TIMEFORCOFFEE' => 'its time for a coffee',

        'HELP_TRWSCHEDULER_INPROGRESS' => 'This task must be performed finished before a new job can be started.',
        'HELP_TRWSCHEDULER_RESET'      => 'Canceling the script during the run is not recommended. Already changed data can not be undone. If Option &quot;Check if Task Scheduler is already running&quot; are active, and if task canceled, the run of the task is set to start.',
    ]
);
