<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwschedulerruntime' => 'Runtimes',
    'SHOP_MODULE_GROUP_trwschedulerdebug'   => 'Debug-Modus',
    'SHOP_MODULE_GROUP_trwschedulermanual'  => 'Manual Run',
    'SHOP_MODULE_GROUP_trwschedulercron'    => 'Cronjob',

    'SHOP_MODULE_bTRWSchedulerCheckStillRunning'      => 'Check if Task Scheduler is already running',
    'HELP_SHOP_MODULE_bTRWSchedulerCheckStillRunning' => 'In live operation, planner tasks should be executed one after the other in order not to jeopardize dependencies.',
    'SHOP_MODULE_bTRWSchedulerCronjobActive'          => 'Enable cron job processing? If set, the task planners cronjob tasks are processed.',
    'SHOP_MODULE_iTRWSchedulerRunTimeBrowser'         => 'Maximum-Execution-Time for Browsers in seconds (Default: 30)',
    'SHOP_MODULE_iTRWSchedulerRunTimeConsole'         => 'Maximum-Execution-Time for Console-Scripts in seconds (Default: Ask your Serveradmin)',
    'SHOP_MODULE_bTRWSchedulerManualAutoReload'       => 'If a reload is necessary during a manual run, should it be started automatically?',
];
