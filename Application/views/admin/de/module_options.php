<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwschedulerruntime' => 'Laufzeiten',
    'SHOP_MODULE_GROUP_trwschedulerdebug'   => 'Debug-Modus',
    'SHOP_MODULE_GROUP_trwschedulermanual'  => 'Manueller Lauf',
    'SHOP_MODULE_GROUP_trwschedulercron'    => 'Cronjob',

    'SHOP_MODULE_bTRWSchedulerCheckStillRunning'      => 'Prüfe ob Aufgabenplaner bereits läuft',
    'HELP_SHOP_MODULE_bTRWSchedulerCheckStillRunning' => 'Im Live-Betrieb sollten Planer-Aufgaben hintereinander ausgeführt werden um Abhängigkeiten nicht zu gefährden.',
    'SHOP_MODULE_bTRWSchedulerCronjobActive'          => 'Cronjobverarbeitung aktivieren? Wenn Option gesetzt, werden die Cronjob-Tasks des Aufgabenplaners verarbeitet.',
    'SHOP_MODULE_iTRWSchedulerRunTimeBrowser'         => 'Maximale Ausführungszeit für Browser-Scripte in Sekunden (Standard: 30)',
    'SHOP_MODULE_iTRWSchedulerRunTimeConsole'         => 'Maximale Ausführungszeit für Serverkonsolen-Scripte in Sekunden (Standard: Beim Admin erfragen)',
    'SHOP_MODULE_bTRWSchedulerManualAutoReload'       => 'Wenn bei einem manuellem Lauf ein Reload notwendig ist, soll dieser automatisch starten?',
];
