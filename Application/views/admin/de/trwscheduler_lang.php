<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

include_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR .
    'de' .
    DIRECTORY_SEPARATOR . basename(__FILE__);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array_merge(
    $aLang,
    [
        'mxtrwscheduler' => 'Aufgabenplaner',

        'TRWSCHEDULER'                      => 'Aufgabenplaner',
        'TRWSCHEDULER_LOG'                  => 'Aufgabenplaner-Log',
        'TRWSCHEDULER_LOG_SHOW'             => 'Datenbank-Log anzeigen',
        'TRWSCHEDULER_LOG_SHOW_PAGE'        => 'Datenbank-Log anzeigen Seite',
        'TRWSCHEDULER_LOG_CLEAN'            => 'Datenbank-Log leeren',
        'TRWSCHEDULER_LOG_MAIL_SUBJECT'     => 'Aufgabenplaner: wichtige Information',
        'TRWSCHEDULER_SEARCH_TASKS'         => 'neue Aufgaben suchen',
        'TRWSCHEDULER_DELETE_TASKS'         => 'entferne alle nicht aktiven Aufgaben',
        'TRWSCHEDULER_OPTIONS'              => 'Task-Optionen',
        'TRWSCHEDULER_TASKS'                => 'Aufgaben',
        'TRWSCHEDULER_BUSY'                 => 'Der Aufgabenplaner ist gerade mit der Aufgabe "%s" beschäftigt. Darum ist ein manueller Start von Aufgaben nicht möglich.',
        'TRWSCHEDULER_INPROGRESS_MANUAL'    => 'Der Aufgabenplaner ist mit einer manuell gestarteten Aufgabe beschäftigt und wird sich dabei ggf. selbst neu starten.<br />Bitte haben Sie einen Augenblick Geduld und verfolgen dabei die Log-Ausgabe.',
        'TRWSCHEDULER_OUTPUT'               => 'Ausgabe:',
        'TRWSCHEDULER_OUTPUT_ENDED'         => 'Ausgabe beendet.',
        'TRWSCHEDULER_CORESETTINGS'         => 'Grundeinstellungen',
        'TRWSCHEDULER_START_TASK_MANUALLY'  => 'manuell starten',
        'TRWSCHEDULER_RELOAD_TASK_MANUALLY' => 'manuell weitermachen',
        'TRWSCHEDULER_CRONJOB'              => 'Cronjob',
        'TRWSCHEDULER_CRONTAB'              => 'Crontab',
        'TRWSCHEDULER_CRONTAB_HELP'         => 'Format * * * * *, entspricht dem klassischem Crontabformat. Die tatsächliche Ausführungszeit kann abweichen und hängt vom Server-Cronjob ab, der den Aufgabenplaner regelmäßig anstößt.',
        'TRWSCHEDULER_CRONTAB_NOT_VALID'    => 'Crontab ist nicht valide, auf den Standardwert zurückgesetzt',
        'TRWSCHEDULER_ACTIVE'               => 'Aktiv',
        'TRWSCHEDULER_SET_OPTION'           => 'Optionen gesetzt',
        'TRWSCHEDULER_LIST'                 => 'zur Aufgabenliste',
        'TRWSCHEDULER_RESET'                => 'Abbruch',
        'TRWSCHEDULER_NEXTTASK'             => 'Nächste potenzielle Cronjob-Aufgabe',
        'TRWSCHEDULER_UPLOAD'               => 'hochladen',
        'TRWSCHEDULER_FILEUPLOAD'           => 'Datei hochladen',
        'TRWSCHEDULER_FILEUPLOADED'         => 'hochgeladene Datei(en)',

        'TRWSCHEDULER_TASK_UPDATEPRICES'  => 'Update bevorstehende Preise erfolgt',
        'TRWSCHEDULER_TASK_TIMEFORCOFFEE' => 'Es ist Zeit für einen Kaffee',

        'HELP_TRWSCHEDULER_INPROGRESS' => 'Diese Aufgabe muss erst zuende geführt werden, bevor ein neue Aufgabe gestartet werden kann.',
        'HELP_TRWSCHEDULER_RESET'      => 'Ein Abbruch des Scriptes während des Laufs ist nicht zu empfehlen. Bereits geänderte Daten können dadurch nicht wieder rückgängig gemacht werden. Wenn die Option &quot;Prüfe ob Aufgabenplaner bereits läuft&quot; gesetzt ist, wird bei Abbruch der Lauf der Aufgabe auf Start gesetzt.',
    ]
);
