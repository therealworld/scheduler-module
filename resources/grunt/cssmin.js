module.exports = {
    options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
    },
    target: {
        files: [
            {
                expand: true,
                cwd: '../out/src',
                src: ['*.css', '!*.min.css'],
                dest: '../out/src',
                ext: '.min.css',
                extDot: 'last'
        }
        ]
    }
};
