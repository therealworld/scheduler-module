module.exports = {

    options: {
        processors: [
            require('autoprefixer')({browserlist: ['last 2 versions', 'ie 11']})
        ]
    },
    dist: {
        files: [
            {
                expand: true,
                cwd: '../out/src',
                src: ['*.css', '!*.min.css'],
                dest: '../out/src',
                ext: '.css',
                extDot: 'last'
        }
        ]
    }
};
