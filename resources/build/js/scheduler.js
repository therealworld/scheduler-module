function clickOnElement(elName)
{
    var clickEl = new YAHOO.util.Dom.get(elName);
    clickEl.click();
    showManualNote();
}
function showManualNote()
{
    var elManualNote = 'runmanuallynote';
    YAHOO.util.Dom.removeClass(elManualNote, 'none');
    YAHOO.util.Dom.addClass(elManualNote, 'show');
}
