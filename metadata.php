<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Model\Maintenance as OxMaintenance;
use TheRealWorld\SchedulerModule\Application\Controller\Admin\SchedulerController;
use TheRealWorld\SchedulerModule\Application\Model\Maintenance;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwscheduler',
    'title' => [
        'de' => 'the-real-world - Aufgabenplaner',
        'en' => 'the-real-world - Scheduler',
    ],
    'description' => [
        'de' => 'Der Aufgabenplaner kann regelmäßig anfallende Arbeiten verarbeiten. Er kann per Cronjob angestoßen werden.',
        'en' => 'The task scheduler can handle recurring tasks regularly. It can be triggered by a cronjob.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwscheduler'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\SchedulerModule\Core\SchedulerEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\SchedulerModule\Core\SchedulerEvents::onDeactivate',
    ],
    'extend' => [
        // Model
        OxMaintenance::class => Maintenance::class,
    ],
    'controllers' => [
        'SchedulerController' => SchedulerController::class,
    ],
    'blocks' => [
        [
            'template' => 'headitem.tpl',
            'block'    => 'admin_headitem_inccss',
            'file'     => 'Application/views/blocks/admin_headitem_inccss.tpl',
        ],
        [
            'template' => 'headitem.tpl',
            'block'    => 'admin_headitem_incjs',
            'file'     => 'Application/views/blocks/admin_headitem_incjs.tpl',
        ],
    ],
    'templates' => [
        'scheduler.tpl' => 'trw/trwscheduler/Application/views/admin/tpl/scheduler.tpl',
    ],
    'settings' => [
        [
            'group' => 'trwschedulercron',
            'name'  => 'bTRWSchedulerCronjobActive',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwschedulerruntime',
            'name'  => 'iTRWSchedulerRunTimeBrowser',
            'type'  => 'num',
            'value' => 30,
        ],
        [
            'group' => 'trwschedulerruntime',
            'name'  => 'iTRWSchedulerRunTimeConsole',
            'type'  => 'num',
            'value' => 150,
        ],
        [
            'group' => 'trwschedulerruntime',
            'name'  => 'bTRWSchedulerCheckStillRunning',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwschedulermanual',
            'name'  => 'bTRWSchedulerManualAutoReload',
            'type'  => 'bool',
            'value' => true,
        ],
        // These options are invisible because the "group" option is null
        [
            'group' => null,
            'name'  => 'bTRWSchedulerStillRunning',
            'type'  => 'bool',
            'value' => false,
        ],
    ],
];
