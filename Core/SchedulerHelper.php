<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SchedulerModule\Core;

use TheRealWorld\SchedulerModule\Application\Model\SchedulerTask;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class SchedulerHelper
{
    /** delete the Task */
    public static function deleteTask($sTask = 'TaskDummy'): void
    {
        if ($sOxId = ToolsDB::getAnyId('trwschedulertasks', ['oxtask' => $sTask])) {
            $oScheduleTask = oxNew(SchedulerTask::class);
            $oScheduleTask->load($sOxId);
            $oScheduleTask->delete();
        }
    }
}
