<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SchedulerModule\Core;

/**
 * Interface for SchedulerTasks.
 */
interface ISchedulerTask
{
    /** Returns the crontab of the Task */
    public function getDefaultCrontab(): string;

    /** get Path for "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getPathForManualFiles(string $sPathKey = ''): string;

    /**
     * get List of "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getManualFileList(string $sPathKey = ''): array;

    /** Install the Task */
    public function install(): bool;

    /**
     * Run the Task.
     *
     * @param int  $iCurrentStep    - The number of the current step
     * @param int  $iCurrentSupStep - The number of the current sub step
     * @param bool $bRunManually    - run the task manually?
     */
    public function run(int $iCurrentStep = 0, int $iCurrentSupStep = 0, bool $bRunManually = false): bool;

    /**
     * Get the number of the next step.
     *
     * @param bool $bSub - The Sub-Step?
     */
    public function getNextStep(bool $bSub = false): int;
}
