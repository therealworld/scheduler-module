<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SchedulerModule\Core;

use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class SchedulerEvents
{
    /**
     * OXID-Core.
     */
    public static function onActivate()
    {
        $sCreateSql = "create table if not exists `trwschedulertasks` (
            `OXID` char(32) character set latin1 collate latin1_general_ci not null,
            `OXSHOPID` int(11) NOT NULL DEFAULT '1' COMMENT 'Shop id (oxshops)',
            `OXACTIVE` tinyint unsigned not null default '1' comment 'Task active?',
            `OXMANUALLY` tinyint unsigned not null default '1' comment 'Task start manually?',
            `OXTASK` varchar(255) character set latin1 collate latin1_general_ci not null default '' comment 'Task ClassName',
            `OXNEXTSTART` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' comment 'The calculate timestamp for the next start',
            `OXINPROGRESS` tinyint unsigned not null default '0' comment 'is Task in progress',
            `OXCURRENTSTEP` smallint(5) unsigned not null default '0' comment 'The current step in the Task',
            `OXCURRENTSUBSTEP` int unsigned not null default '0' comment 'The current substep of the current step in the Task',
            `OXMODULEPATH` varchar(255) character set latin1 collate latin1_general_ci not null default '' comment 'Module-Path for Task',
            `OXNAMESPACECLASSNAME` varchar(255) character set latin1 collate latin1_general_ci not null default '' comment 'NamespaceClassName for Task',
            `OXCRONTAB` varchar(255) character set latin1 collate latin1_general_ci not null default '' comment 'Crontab for Task',
            `OXTIMESTAMP` timestamp not null default current_timestamp on update current_timestamp,
            primary key (`OXID`)
            ) engine=InnoDB default CHARSET=utf8 comment 'the-real-world: Schedule-Tasks'";

        ToolsDB::execute($sCreateSql);

        // additional Module-Update v1.6
        if (!ToolsDB::tableColumnExists('trwschedulertasks', 'OXSHOPID')) {
            $sAddSql = "ALTER TABLE `trwschedulertasks` ADD `OXSHOPID` int(11) NOT NULL DEFAULT '1' COMMENT 'Shop id (oxshops)'";
            ToolsDB::execute($sAddSql);
        }

        return true;
    }

    /**
     * OXID-Core.
     */
    public static function onDeactivate()
    {
        // for security set return true
        return true;
        $sDeleteSql = 'drop table if exists `trwschedulertasks`';
        ToolsDB::execute($sDeleteSql);

        // delete Tasks
        $aTasks = ['TaskDummy', 'TaskUpdateUpcomingPrices'];
        foreach ($aTasks as $sTask) {
            SchedulerHelper::deleteTask($sTask);
        }

        return true;
    }
}
